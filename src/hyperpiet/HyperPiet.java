package hyperpiet;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.awt.image.WritableRaster;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javax.imageio.ImageIO;

public class HyperPiet extends Application {
    private static final Rectangle2D bounds = Screen.getPrimary().getVisualBounds(); // screen bounds
    
    // palette
    public static final String[] TEXT_COLS = {"#222222", "#AAAAAA", "white"};
    public static final String[] COL_NAMES = {"red", "yellow", "green", "cyan", "blue", "magenta"};
    public static final String[] SHADES = {"light ", "", "dark "};
    public static final String[][] COLS = {
        {"#FFC0C0", "#FF0000", "#C00000"},
        {"#FFFFC0", "#FFFF00", "#C0C000"},
        {"#C0FFC0", "#00FF00", "#00C000"},
        {"#C0FFFF", "#00FFFF", "#00C0C0"},
        {"#C0C0FF", "#0000FF", "#0000C0"},
        {"#FFC0FF", "#FF00FF", "#C000C0"}
    };
    
    private static final int DEF_GRID_SIZE = 10;
    
    public static final int CHART_SIZE = (int)(bounds.getHeight() * 0.7); // canvas
    
    public static Col[][] colData = new Col[6][3]; // Col objects for COLS
        
    // main text fields
    public static TextArea stdinF, stdoutF;
    public static TextField stdoutInfoF;
    
    private float cellSize;
    private static GridPane chartGrid;
    private static Grid grid;
    private static File curFile;

    public static ArrayList<ArrayList<Pair>> gridData = new ArrayList<ArrayList<Pair>>(); // current state of grid
    private ArrayList<ArrayList<Rectangle>> gridBtns = new ArrayList<ArrayList<Rectangle>>(); // palette

    public static Col curCol = Col.WHITE;
    
    // palette element instantiation
    private static Button makePicker(int i, int j) {
        final int i0 = i, j0 = j;
        
        Button picker = new Button();
        picker.setText(SHADES[j] + COL_NAMES[i]);
        picker.setMinSize(120, 30);
        picker.setMaxSize(120, 30);
        picker.setStyle(
            "-fx-background-color:" + COLS[i][j] + ";" +
            "-fx-text-fill:" + TEXT_COLS[j]
        );
        picker.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                curCol = colData[i0][j0];
            }
        });
        
        chartGrid.add(picker, i, j);
        
        return picker;
    }
    
    private static MenuBar mb;
    private static TextField widthTF, heightTF;
    
    private void open(Stage s) {
        final FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(System.getProperty("user.home")));
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("Portable network graphics", "*.png"));

        File f = fc.showOpenDialog(s);

        if (f != null) {
            curFile = f;
            grid.load(curFile);
            widthTF.setText(Integer.toString(grid.w));
            heightTF.setText(Integer.toString(grid.h));

            s.setTitle(curFile.getAbsoluteFile().getName() + " - HyperPiet");
            mb.setStyle("-fx-background-color:#EEFFDD");
        }
    }
    
    private void save(Stage s) {
        if (curFile == null) {
            saveAs(s);
            return;
        }
        
        grid.save(curFile);
        mb.setStyle("-fx-background-color:#EEFFDD");
    }
    
    private void saveAs(Stage s) {
        final FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(System.getProperty("user.home")));
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("Portable Network Graphics", "*.png"));

        File f = fc.showSaveDialog(s);

        if (f != null) {
            curFile = f;
            grid.save(curFile);
            s.setTitle(curFile.getAbsoluteFile().getName() + " - HyperPiet");
            mb.setStyle("-fx-background-color:#EEFFDD");
        }        
    }
    
    @Override
    public void start(Stage primaryStage) {
        
        // instantiate colour objects
        
        for (int i=0; i<6; ++i) {            
            for (int j=0; j<3; ++j) {
                colData[i][j] = new Col(COLS[i][j], i, j);
            }
        }
        
        // main layout
        
        StackPane sp = new StackPane();
        
        /*Pane p = new Pane();
        
        p.setStyle(
            "-fx-background-image:url('http://cluelesscritic.com/wp-content/uploads/2017/11/maxresdefault.jpg');" +
            "-fx-background-position:center center;" +
            "-fx-opacity:0.1;" +
            "-fx-background-size:100% auto"
        );
        
        sp.getChildren().add(p);*/
        
        BorderPane bp = new BorderPane();
        
        sp.getChildren().add(bp);
        
        VBox lb = new VBox();
        lb.setMinWidth(bounds.getWidth() / 2);
        lb.setAlignment(Pos.CENTER);
        
        bp.setLeft(lb);
                
        cellSize = CHART_SIZE / DEF_GRID_SIZE;
        grid = new Grid(lb, DEF_GRID_SIZE, DEF_GRID_SIZE, cellSize);
        
        // palette grid
        
        chartGrid = new GridPane();
        chartGrid.setHgap(1);
        chartGrid.setVgap(1);
        chartGrid.setAlignment(Pos.TOP_CENTER);
        chartGrid.setPadding(new Insets(10));
        
        // instantiate colour pickers
        
        for (int i=0; i<6; ++i) {            
            for (int j=0; j<3; ++j) {
                makePicker(i, j);
            }
        }
        
        // white picker instantiation
        
        Button whitePicker = new Button();
        whitePicker.setText("white");
        whitePicker.setMinSize(362, 30);
        whitePicker.setMaxSize(362, 30);
        whitePicker.setStyle(
            "-fx-background-color:white;" +
            "-fx-text-fill:dimgray"
        );
        whitePicker.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                curCol = Col.WHITE;
            }
        });
        
        chartGrid.add(whitePicker, 0, 3, 3, 1);
        
        // black picker instantiation
        
        Button blackPicker = new Button();
        blackPicker.setText("black");
        blackPicker.setMinSize(362, 30);
        blackPicker.setMaxSize(362, 30);
        blackPicker.setStyle(
            "-fx-background-color:black;" + 
            "-fx-text-fill:lightgray"
        );
        blackPicker.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                curCol = Col.BLACK;
            }
        });
        
        chartGrid.add(blackPicker, 3, 3, 3, 1);
        
        // layout for config. options
        
        GridPane settingsGrid = new GridPane();
        settingsGrid.setAlignment(Pos.CENTER);
        settingsGrid.setHgap(40);
        
        HBox gridSizeBox = new HBox();
        gridSizeBox.setSpacing(10);
        gridSizeBox.setAlignment(Pos.CENTER);
        
        settingsGrid.add(gridSizeBox, 0, 0, 2, 1);
        
        // grid size setting
        
        gridSizeBox.getChildren().add(new Text("Grid size"));
        
        widthTF = new TextField(Integer.toString(DEF_GRID_SIZE));
        widthTF.setMaxWidth(50);
        
        gridSizeBox.getChildren().add(widthTF);
        gridSizeBox.getChildren().add(new Text("x"));
        
        heightTF = new TextField(Integer.toString(DEF_GRID_SIZE));
        heightTF.setMaxWidth(50);
        
        gridSizeBox.getChildren().add(heightTF);
        
        // update setting (no auto update)
        
        Button updateBtn = new Button("Update grid");
        updateBtn.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) { grid.update(Integer.parseInt(widthTF.getText()), Integer.parseInt(heightTF.getText())); }
        });
        
        // refresh setting
        
        Button clearBtn = new Button("Clear grid");
        clearBtn.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) { grid.clear(); }
        });
        
        // file IO for canvas        
        Button openBtn = new Button("Open");
        openBtn.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) { open(primaryStage); }
        });
        
        HBox openBox = new HBox();
        openBox.setSpacing(10);
        openBox.setAlignment(Pos.CENTER);
        
        settingsGrid.add(openBox, 4, 0);
        
        openBox.getChildren().add(openBtn);
        
        Button saveBtn = new Button("Save");
        saveBtn.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) { save(primaryStage); }
        });
                
        settingsGrid.add(updateBtn, 2, 0);
        settingsGrid.add(clearBtn, 3, 0);
        settingsGrid.add(saveBtn, 5, 0);
        
        lb.getChildren().add(settingsGrid);
        
        // STDIN for programs
        
        VBox rb = new VBox();
        rb.setMinWidth(bounds.getWidth() / 2);
        rb.setAlignment(Pos.CENTER);
        
        bp.setRight(rb);
        
        Label stdinText = new Label("STDIN");
        stdinText.setPadding(new Insets(20, 0, 0, 0));
        
        rb.getChildren().add(stdinText);
        
        stdinF = new TextArea();
        stdinF.setMinSize(CHART_SIZE, CHART_SIZE * 0.3);
        stdinF.setMaxSize(CHART_SIZE, CHART_SIZE * 0.3);
        stdinF.setWrapText(true);
        
        rb.getChildren().add(stdinF);
        
        // STDOUT for programs
        
        Label stdoutText = new Label("STDOUT");
        stdoutText.setPadding(new Insets(40, 0, 0, 0));
        
        rb.getChildren().add(stdoutText);
        
        stdoutF = new TextArea();
        stdoutF.setMinSize(CHART_SIZE, CHART_SIZE * 0.6);
        stdoutF.setMaxSize(CHART_SIZE, CHART_SIZE * 0.6);
        stdoutF.setEditable(false);
        stdoutF.setWrapText(true);
        
        rb.getChildren().add(stdoutF);
        
        stdoutInfoF = new TextField();
        stdoutInfoF.setMinSize(CHART_SIZE, CHART_SIZE * 0.05);
        stdoutInfoF.setMaxSize(CHART_SIZE, CHART_SIZE * 0.05);
        stdoutInfoF.setEditable(false);
        
        rb.getChildren().add(stdoutInfoF);
        
        HBox rHBox = new HBox();
        
        rb.getChildren().add(rHBox);
        
        // execution tools layout
        
        Button runBtn = new Button("Run");
        runBtn.setMinSize(90, 30);
        runBtn.setOnAction(new EventHandler<ActionEvent>() {
           
            @Override
            public void handle(ActionEvent event) {
                stdoutInfoF.setText("Running the program...");
                Interp.interp(grid);
            }
        });
        
        rHBox.getChildren().add(runBtn);
        rHBox.setMinWidth(bounds.getWidth() / 2);
        rHBox.setAlignment(Pos.TOP_CENTER);
        rHBox.setPadding(new Insets(10));
        rHBox.setSpacing(20);
        
        Button timeoutBtn = new Button("Set Timeout");
        timeoutBtn.setMinSize(90, 30);
        timeoutBtn.setOnAction(new EventHandler<ActionEvent> () {
           
            @Override
            public void handle(ActionEvent event) {
                
                // uses a separate dialog to streamline main window GUI
                
                final Stage dialog = new Stage();
                dialog.initModality(Modality.APPLICATION_MODAL);
                dialog.initOwner(primaryStage);
                dialog.initStyle(StageStyle.UTILITY);
                
                VBox dialogBox = new VBox();
                dialogBox.setPadding(new Insets(20));
                dialogBox.setSpacing(20);
                dialogBox.setAlignment(Pos.CENTER_LEFT);
                
                HBox limitBox = new HBox();
                limitBox.setAlignment(Pos.TOP_LEFT);
                
                TextField limitF = new TextField(Integer.toString(Interp.timeout));
                limitF.setMaxWidth(80);
                limitF.setEditable(Interp.willTimeout);
                limitF.textProperty().addListener(new ChangeListener<String>() {
                    
                    @Override
                    public void changed(ObservableValue<? extends String> obs, String prev, String cur) {
                        Interp.timeout = Integer.parseInt(cur);
                    }
                });
                
                ToggleGroup group = new ToggleGroup();
                
                RadioButton limitOp = new RadioButton("Limit to (ms): ");
                limitOp.setToggleGroup(group);
                limitOp.setSelected(Interp.willTimeout);
                limitOp.selectedProperty().addListener(new ChangeListener<Boolean>() {
                   
                    @Override
                    public void changed(ObservableValue<? extends Boolean> obs, Boolean prevSelected, Boolean curSelected) {
                        limitF.setEditable(curSelected);
                        Interp.willTimeout = curSelected;
                        
                        System.out.println(Interp.willTimeout);
                    }
                });
                
                limitBox.getChildren().add(limitOp);
                limitBox.getChildren().add(limitF);
                
                RadioButton unlimitOp = new RadioButton("Unlimited (not recommended)");
                unlimitOp.setToggleGroup(group);
                unlimitOp.setSelected(!Interp.willTimeout);
                
                dialogBox.getChildren().add(limitBox);
                dialogBox.getChildren().add(unlimitOp);
                
                Scene scene = new Scene(dialogBox, 300, 100);
                dialog.setTitle("Timeout");
                dialog.setScene(scene);
                dialog.show();
            }
        });
        
        rHBox.getChildren().add(timeoutBtn);
        
        lb.getChildren().add(chartGrid);
        
        // main scene
        
        mb = new MenuBar();
        mb.setMaxHeight(10);
        mb.setStyle("-fx-background-color:#FF8888");
        
        Menu file = new Menu("File");
        
        MenuItem open = new MenuItem("Open");
        MenuItem sav = new MenuItem("Save"), saveAs = new MenuItem("Save As");
        MenuItem fOps = new MenuItem("Options");
        
        open.setOnAction(e -> { open(primaryStage); });
        open.setAccelerator(new KeyCodeCombination(KeyCode.O, KeyCodeCombination.CONTROL_DOWN));
        sav.setOnAction(e -> { save(primaryStage); });
        sav.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCodeCombination.CONTROL_DOWN));
        saveAs.setOnAction(e -> { saveAs(primaryStage); });
        saveAs.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCodeCombination.SHIFT_DOWN, KeyCodeCombination.CONTROL_DOWN));
        fOps.setOnAction(e -> {
            final Stage dialog = new Stage();
            dialog.initModality(Modality.APPLICATION_MODAL);
            dialog.initOwner(primaryStage);
            dialog.initStyle(StageStyle.UTILITY);
            
            VBox dialogBox = new VBox();
            dialogBox.setPadding(new Insets(20));
            dialogBox.setSpacing(20);
            dialogBox.setAlignment(Pos.CENTER_LEFT);
            
            //
            
            Scene scene = new Scene(dialogBox, 300, 100);
            dialog.setTitle("File Options");
            dialog.setScene(scene);
            dialog.show();
        });
        fOps.setAccelerator(new KeyCodeCombination(KeyCode.O, KeyCodeCombination.SHIFT_DOWN, KeyCodeCombination.CONTROL_DOWN));
        
        file.getItems().addAll(open, sav, saveAs, fOps);
        
        Menu grd = new Menu("Grid");
                
        MenuItem clear = new MenuItem("Clear"), resize = new MenuItem("Resize"), res = new MenuItem("Resolution");
        
        clear.setOnAction(e -> { grid.clear(); });
        clear.setAccelerator(new KeyCodeCombination(KeyCode.C, KeyCodeCombination.SHIFT_DOWN, KeyCodeCombination.CONTROL_DOWN));
        resize.setOnAction(e -> {
            final Stage dialog = new Stage();
            dialog.initModality(Modality.APPLICATION_MODAL);
            dialog.initOwner(primaryStage);
            dialog.initStyle(StageStyle.UTILITY);
            
            VBox dialogBox = new VBox();
            dialogBox.setPadding(new Insets(20));
            dialogBox.setSpacing(20);
            dialogBox.setAlignment(Pos.CENTER_LEFT);
            
            HBox widthBox = new HBox(new Text("Width:  "));
            widthBox.setAlignment(Pos.CENTER);
            
            TextField wbF = new TextField(Integer.toString(grid.w));
            wbF.setMaxWidth(50);
            
            widthBox.getChildren().add(wbF);
            
            HBox heightBox = new HBox(new Text("Height:  "));
            heightBox.setAlignment(Pos.CENTER);
            
            TextField hbF = new TextField(Integer.toString(grid.h));
            hbF.setMaxWidth(50);
            
            heightBox.getChildren().add(hbF);
            
            dialogBox.getChildren().addAll(widthBox, heightBox);
            
            Scene scene = new Scene(dialogBox, 200, 100);
            dialog.setTitle("File Options");
            dialog.setScene(scene);
            dialog.show();
        });
        resize.setAccelerator(new KeyCodeCombination(KeyCode.R, KeyCodeCombination.CONTROL_DOWN));
        res.setOnAction(e -> {
            final Stage dialog = new Stage();
            dialog.initModality(Modality.APPLICATION_MODAL);
            dialog.initOwner(primaryStage);
            dialog.initStyle(StageStyle.UTILITY);
            
            VBox dialogBox = new VBox();
            dialogBox.setPadding(new Insets(20));
            dialogBox.setSpacing(20);
            dialogBox.setAlignment(Pos.CENTER_LEFT);
            
            HBox curBox = new HBox(new Text("Current resolution:  "));
            curBox.setAlignment(Pos.CENTER);
            
            TextField curTF = new TextField("1");
            curTF.setMaxWidth(50);
            
            curBox.getChildren().add(curTF);
            
            HBox targetBox = new HBox(new Text("Target resolution:  "));
            targetBox.setAlignment(Pos.CENTER);
            
            TextField targetTF = new TextField("1");
            targetTF.setMaxWidth(50);
            
            targetBox.getChildren().add(targetTF);
            
            HBox buttonBox = new HBox();
            buttonBox.setAlignment(Pos.CENTER);
            
            Button apply = new Button("Apply");
            apply.setOnAction(ev -> {
                int target = Integer.parseInt(targetTF.getText());
                int cur = Integer.parseInt(curTF.getText());
                
                System.out.println(cur);
                
                System.out.println(target);
                System.out.println(targetTF.getText());
                System.out.println(Integer.parseInt(targetTF.getText()));
                System.out.println(Integer.parseInt(curTF.getText()));
                
                double r = (double)target / (double)cur;
                
                System.out.println(r);
                
                int nw = (int)(grid.w * r), nh = (int)(grid.h * r);
                      
                if (nw > grid.w && nh > grid.h) {
                    int pw = grid.w, ph = grid.h;
                    
                    grid.update(nw, nh);
                    
                    for (int i=ph-1; i>=0; --i) {
                        for (int j=pw-1; j>=0; --j) {
                            Pair t = grid.data.get((int)(i)).get((int)(j)).p;
                            
                            for (int m=0; m<r; ++m) {
                                for (int n=0; n<r; ++n) {
                                    Grid.Datum d = grid.data.get(i * (int)r + m).get(j * (int)r + n);
                                    d.cell.setStyle("-fx-fill:#" + Integer.toHexString(Grid.pairToByte(t)).toUpperCase().substring(2) + ";");
                                    d.p.a = t.a;
                                    d.p.b = t.b;
                                }
                            }
                        }
                    }
                } else if (nw < grid.w && nh < grid.h) {
                    for (int i=0; i<nh; ++i) {
                        for (int j=0; j<nw; ++j) {
                            Pair t = grid.data.get((int)(i / r)).get((int)(j / r)).p;
                            Grid.Datum d = grid.data.get(i).get(j);
                            d.cell.setStyle("-fx-fill:#" + Integer.toHexString(Grid.pairToByte(t)).toUpperCase().substring(2) + ";");
                            d.p.a = t.a;
                            d.p.b = t.b;
                        }
                    }
                    
                    grid.update(nw, nh);
                }
                
                widthTF.setText(Integer.toString(nw));
                heightTF.setText(Integer.toString(nh));
                
                dialog.fireEvent(new WindowEvent(dialog, WindowEvent.WINDOW_CLOSE_REQUEST));
            });
            
            buttonBox.getChildren().add(apply);
            
            dialogBox.getChildren().addAll(curBox, targetBox, buttonBox);
                    
            Scene scene = new Scene(dialogBox, 300, 150);
            dialog.setTitle("Resolution Options");
            dialog.setScene(scene);
            dialog.show();
        });
        res.setAccelerator(new KeyCodeCombination(KeyCode.R, KeyCodeCombination.SHIFT_DOWN, KeyCodeCombination.CONTROL_DOWN));
        
        grd.getItems().addAll(clear, resize, res);
        
        Menu run = new Menu("Run");
        
        MenuItem interp = new MenuItem("Run Program"), timeout = new MenuItem("Set Timeout");
        interp.setOnAction(null);
        interp.setAccelerator(new KeyCodeCombination(KeyCode.P, KeyCodeCombination.CONTROL_DOWN));
        timeout.setOnAction(null);
        timeout.setAccelerator(new KeyCodeCombination(KeyCode.T, KeyCodeCombination.CONTROL_DOWN));
        
        run.getItems().addAll(interp, timeout);
        
        mb.getMenus().addAll(file, grd, run);
        
        VBox vb = new VBox(mb);
        vb.getChildren().add(sp);
                
        Image img = new Image(getClass().getResourceAsStream("bg.png"));
        sp.setBackground(new Background(new BackgroundImage(img, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.CENTER, new BackgroundSize(100, 100, true, true, true, true))));
        
        Scene scene = new Scene(vb, 1600, 1200);
        
        primaryStage.setMaximized(true);
        primaryStage.setTitle("NO FILE - HyperPiet");
        primaryStage.getIcons().add(new Image(HyperPiet.class.getResourceAsStream("icon.png")));
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}