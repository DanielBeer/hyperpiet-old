package hyperpiet;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import javafx.scene.shape.Rectangle;

import java.util.ArrayList;
import java.util.HashMap;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.StrokeType;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;

public class Grid {
    
    // instance variables
    GridPane pane;
    int w, h;
    ArrayList<ArrayList<Datum>> data;
    

    public class Datum {
        Rectangle cell;
        Pair p;
        
        public Datum(Grid parent, float cellSize, int i, int j) {
            final int i0 = i, j0 = j;
            
            cell = new Rectangle(cellSize, cellSize);
            cell.setStyle("-fx-fill:#FFFFFF;");
            cell.setStrokeType(StrokeType.INSIDE);
            cell.setStrokeWidth(0.5);
            cell.setStroke(Color.LIGHTGRAY);

            cell.setOnMousePressed(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent click) {
                    if (click.isPrimaryButtonDown()) {
                        if (click.getClickCount() == 1) {

                            // set cell colour to current colour
                            cell.setStyle("-fx-fill:" + HyperPiet.curCol.getCol());
                            parent.data.get(i0).get(j0).p = HyperPiet.curCol.getProps();
                        } else if (click.getClickCount() == 2) {

                            // reset cell colour to white
                            cell.setStyle("-fx-fill:#FFFFFF");
                            parent.data.get(i0).get(j0).p = Col.WHITE.getProps();
                        }
                    } else if (click.isSecondaryButtonDown()) {

                        // hue shift

                        Pair props = parent.data.get(i0).get(j0).p;

                        if (props.a < 6) {
                            int darkness = props.b + 1;

                            if (darkness == 3) darkness = 0;

                            cell.setStyle("-fx-fill:" + HyperPiet.COLS[props.a][darkness]);
                            parent.data.get(i0).get(j0).p.b = darkness;
                        }
                    }
                }
            });

            parent.pane.add(cell, i, j);
            
            p = Col.WHITE.getProps();
        }
    }
    
    // constructor
    public Grid(Pane parent, int wi, int ht, float cellSize) {
        pane = new GridPane();
        pane.setPadding(new Insets(20));
        pane.setAlignment(Pos.TOP_CENTER);
        
        parent.getChildren().add(pane);
        
        data = new ArrayList<ArrayList<Datum>>();
        w = wi;
        h = ht;
        
        for (int i=0; i<h; ++i) {
            ArrayList<Datum> row = new ArrayList<Datum>();
            
            for (int j=0; j<w; ++j) {
                row.add(new Datum(this, cellSize, j, i));
            }
            
            data.add(row);
        }
    }
    
    
    public void clear() {
        for (int i=0; i<this.h; ++i) {
            for (int j=0; j<this.w; ++j) {
                Datum d = this.data.get(j).get(i);
                d.cell.setStyle("-fx-fill:#FFFFFF;");
                d.p.a = Col.WHITE.hue;
                d.p.b = Col.WHITE.darkness;
            }
        }
    }
    
    
    public void update(int nw, int nh) {        
        if (nw == this.w && nh == this.h) return;
        
        final float newCellSize = HyperPiet.CHART_SIZE / Math.max(nw, nh);
        
        for (int i=0; i<Math.max(nh, this.h); ++i) {
            if (i < this.h) {
                if (i < nh) {
                    for (int j=0; j<Math.max(nw, this.w); ++j) {
                        if (j < this.w) {
                            if (j < nw) {
                                final Rectangle c = this.data.get(i).get(j).cell;
                                c.setWidth(newCellSize);
                                c.setHeight(newCellSize);
                            } else {
                                this.data.get(i).get(nw);
                                this.pane.getChildren().remove(this.data.get(i).get(nw).cell);
                                this.data.get(i).remove(nw);
                            }
                        } else {
                            this.data.get(i).add(new Datum(this, newCellSize, j, i));
                        }
                    }
                } else {
                    for (int k=this.w-1; k>=0; --k) {
                        this.data.get(nh).get(k);
                        this.pane.getChildren().remove(this.data.get(nh).get(k).cell);
                    }
                    
                    this.data.remove(nh);
                }
            } else {
                ArrayList<Datum> row = new ArrayList<Datum>();
                
                for (int j=0; j<Math.max(nw, this.w); ++j) {
                    row.add(new Datum(this, newCellSize, j, i));
                }
                
                this.data.add(row);
            }
        }
        
        this.w = nw;
        this.h = nh;
    }
    
    private static HashMap<String, Pair> lookup = new HashMap<String, Pair>();
    private static boolean init = false;
    
    private static void makeLookup() {
        for (int i=0; i<6; ++i) {
            for (int j=0; j<3; ++j) {
                lookup.put(HyperPiet.COLS[i][j], HyperPiet.colData[i][j].getProps());
            }
        }
        
        lookup.put("#000000", Col.BLACK.getProps());
        lookup.put("#FFFFFF", Col.WHITE.getProps());
    }
    
    
    private static Pair intToPair(int c) {
        String hex = "#" + Integer.toHexString(c).toUpperCase().substring(2);
        
        return lookup.get(hex);
    }
    
    
    public void load(File f) {
        if (!init) {
            makeLookup();
            init = true;
        }

        try {
            BufferedImage bi = ImageIO.read(f);
            
            this.update(bi.getWidth(), bi.getHeight());
            
            for (int i=0; i<bi.getHeight(); ++i) {
                for (int j=0; j<bi.getWidth(); ++j) {
                    int b = bi.getRGB(j, i);
                    
                    Datum cur = this.data.get(i).get(j);
                    Pair p = intToPair(b);
                    
                    cur.p.a = p.a;
                    cur.p.b = p.b;
                    cur.cell.setStyle("-fx-fill:#" + Integer.toHexString(b).toUpperCase().substring(2) + ";");
                }
            }
        } catch (IOException e) { }
    }
    
    
    public static int pairToByte(Pair p) {
        String hex;
        
        if (p.equals(Col.WHITE.getProps())) hex = "FFFFFF";
        else if (p.equals(Col.BLACK.getProps())) hex = "000000";
        else hex = HyperPiet.COLS[p.a][p.b].substring(1);
        
        return 0xFF << 24 | Integer.parseInt(hex, 16);
    }
    
    
    private int[] getBytes() {
        int[] bytes = new int[this.w * this.h];
        
        for (int i=0; i<this.h; ++i) {
            for (int j=0; j<this.w; ++j) {
                bytes[i * this.h + j] = pairToByte(this.data.get(j).get(i).p);
            }
        }
        
        return bytes;
    }    
    
    
    public void save(File f) {
        try {
            BufferedImage bi = new BufferedImage(this.w, this.h, BufferedImage.TYPE_INT_RGB);
                        
            WritableRaster wr = (WritableRaster)bi.getData();
            
            int[] p = new int[3];
            wr.getPixel(0, 0, p);            
            
            int[] pixels = new int[this.w * this.h * 3];
            
            for (int i=0; i<bi.getHeight(); ++i) {
                for (int j=0; j<bi.getWidth(); ++j) {
                    int idx = 3 * (i * bi.getHeight() + j);
                    int b = pairToByte(this.data.get(i).get(j).p);
                    
                    pixels[idx] = b >> 16 & 0xFF;
                    pixels[idx + 1] = b >> 8 & 0xFF;
                    pixels[idx + 2] = b & 0xFF;
                }
            }
            
            wr.setPixels(0, 0, this.w, this.h, pixels);
            bi.setData(wr);

            System.out.println(ImageIO.write(bi, "png", f));
        } catch (IOException e) { }
    }
}