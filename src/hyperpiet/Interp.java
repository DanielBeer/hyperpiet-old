package hyperpiet;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Stack;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

import javafx.concurrent.Task;

public class Interp {
    
    // configuration in HyperPiet.java
    
    private static enum Dir {LEFT, RIGHT};
    
    public static int timeout = 3000; // how long a program will run before it stops
    public static Boolean willTimeout = true; // whether or not to stop a program
    
    private static int gx, gy, dx, dy, lastDx = 0, lastDy = 0, lastHueShift = -1, lastDarkShift = -1; // counters
    private static Dir cc = Dir.RIGHT; // codel chooser
    private static Stack stk;
    private static long startTime;
    private static Boolean hasTimedOut;
        
    private static Pattern nlp = Pattern.compile("\\s+"); // frequently used pattern

    private static ExecutorService es = Executors.newSingleThreadExecutor();
    
    private static Task<Boolean> timeoutCheck;
    private static Task<Boolean> createTimeoutChecker() {
        hasTimedOut = false;
        
        return new Task<Boolean>() { // threading for timeout check while program runs in separate loop
        
            @Override protected Boolean call() throws Exception {
                long curTime;

                for (;;) {
                    if (isCancelled()) {
                        break;
                    }

                    curTime = System.currentTimeMillis();

                    if (willTimeout && curTime - startTime > timeout) {

                        // program timed out

                        HyperPiet.stdoutInfoF.setText("Error: Execution timed out. In ~" + Long.toString((curTime - startTime) / 1000) + "s. Timestamp: " + new Timestamp(curTime).toString());
                        hasTimedOut = true;

                        cancel();

                        break;
                    }

                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException e) {
                        if (isCancelled()) {
                            break;
                        }
                    }
                }

                return true;
            }
        };
    };

    private static void rotateDir(Boolean cw) {
        int tempDy = dy;
        
        if (cw) {
            dy = dx;
            dx = -tempDy;
        } else {
            dy = -dx;
            dx = tempDy;
        }
    }
    
    // checks if there are only white cells extending to an edge in the direction of the DP
    private static Boolean isBlank(Grid grid) {
        ArrayList<ArrayList<Grid.Datum>> data = grid.data;
        
        if (dy == 0) {
            for (int i=gx+dx; i>=0&&i<grid.w; i+=dx) {                    
                if (!(data.get(i).get(gy).p.equals(Col.WHITE.getProps()))) {
                    return false;
                }
            }
        } else {
            for (int i=gy+dy; i>=0&&i<grid.h; i+=dy) {
                if (!(data.get(gx).get(i).p.equals(Col.WHITE.getProps()))) {
                    return false;
                }
            }
        }
        
        return true;
    }
    
    private static boolean[][] traversed; // lookup table for getBlockSize
    
    // get the size of block of adjacent cells of a single colour
    private static int getBlockSize(Grid grid, Pair cur, int gx0, int gy0) {
        int size = 1;
        ArrayList<ArrayList<Grid.Datum>> data = grid.data;
        
        traversed[gx0][gy0] = true; // mark current cell
        
        // move current position to edge of block
        
        if (dy == 0 && gy == gy0) {
            if (dx == 1 && gx0 > gx) gx = gx0;
            if (dx == -1 && gx0 < gx) gx = gx0;
        }
        
        if (dx == 0 && gx == gx0) {
            if (dy == 1 && gy0 > gy) gy = gy0;
            if (dy == -1 && gy0 < gy) gy = gy0;
        }
        
        // check adjacent valid cells
        
        if (gx0 < grid.w - 1 && !traversed[gx0 + 1][gy0] && data.get(gx0 + 1).get(gy0).p.equals(cur)) size += getBlockSize(grid, cur, gx0 + 1, gy0);
        if (gy0 < grid.h - 1 && !traversed[gx0][gy0 + 1] && data.get(gx0).get(gy0 + 1).p.equals(cur)) size += getBlockSize(grid, cur, gx0, gy0 + 1);
        if (gx0 > 0 && !traversed[gx0 - 1][gy0] && data.get(gx0 - 1).get(gy0).p.equals(cur)) size += getBlockSize(grid, cur, gx0 - 1, gy0);
        if (gy0 > 0 && !traversed[gx0][gy0 - 1] && data.get(gx0).get(gy0 - 1).p.equals(cur)) size += getBlockSize(grid, cur, gx0, gy0 - 1);
                
        return size;
    }
    
    private static void traverseWhiteBlock(Grid grid) {
        ArrayList<ArrayList<Grid.Datum>> data = grid.data;
        
        if (dy == 0) {
            if (dx == 1) {
                while (gx < grid.w - 1 && data.get(gx).get(gy).p.equals(Col.WHITE.getProps()) && !data.get(gx + 1).get(gy).p.equals(Col.BLACK.getProps())) {
                    gx += 1;
                    
                    System.out.printf("W %d %d\n", gx, gy);
                }
            } else {
                while (gx > 0 && data.get(gx).get(gy).p.equals(Col.WHITE.getProps()) && !data.get(gx - 1).get(gy).p.equals(Col.BLACK.getProps())) {
                    gx -= 1;
                    
                    System.out.printf("W %d %d\n", gx, gy);
                }
            }
        } else {
            if (dy == 1) {
                while (gy < grid.h - 1 && data.get(gx).get(gy).p.equals(Col.WHITE.getProps()) && !data.get(gx).get(gy + 1).p.equals(Col.BLACK.getProps())) {
                    gy += 1;
                    
                    System.out.printf("W %d %d\n", gx, gy);
                }
            } else {
                while (gy > 0 && data.get(gx).get(gy).p.equals(Col.WHITE.getProps()) && !data.get(gx).get(gy - 1).p.equals(Col.BLACK.getProps())) {
                    gy -= 1;
                    
                    System.out.printf("W %d %d\n", gx, gy);
                }
            }
        }
    }
    
    private static void updateDirPtr(Grid grid) {
        ArrayList<ArrayList<Grid.Datum>> data = grid.data;
        
        if (isBlank(grid) || data.get(gx + dx).get(gy + dy).p.equals(Col.BLACK.getProps())) {
            rotateDir(true);
                        
            updateDirPtr(grid);
        }
    }
    
    private static boolean isNumeric(String s) {  
        return s != null && s.matches("[-+]?\\d*\\.?\\d+");  
    }
    
    public static void interp(Grid grid) {
        ArrayList<ArrayList<Grid.Datum>> data = grid.data;
        startTime = System.currentTimeMillis();
        
        timeoutCheck = createTimeoutChecker();
        es.execute(timeoutCheck);
        
        gx = 0; gy = 0;
        dx = 1; dy = 0;
        stk = new Stack();
        
        Pair first, second;
        int size = 0;
        
        // main execution loop
        
        for (;;) {
            if (hasTimedOut) return;
            
            Pair cur = data.get(gx).get(gy).p;
            
            //System.out.printf("%d %d\n", gx, gy);
            
            for (; cur.equals(Col.WHITE.getProps()); cur=data.get(gx).get(gy).p) {
                traverseWhiteBlock(grid);
                
                System.out.printf("1 D %d %d G %d %d\n", dx, dy, gx, gy);
                
                updateDirPtr(grid);
                
                System.out.printf("2 D %d %d G %d %d\n", dx, dy, gx, gy);
            }
            
            if (!cur.equals(Col.WHITE.getProps())) {
                traversed = new boolean[grid.w][grid.h]; // reset lookup
                size = getBlockSize(grid, cur, gx, gy); // get block size and adjust position
            }
            
            first = data.get(gx).get(gy).p;
            
            try {
                updateDirPtr(grid); // look ahead and change direction as needed
            } catch (StackOverflowError e) {
                
                // NOTE: needs more looking into as to why this occurs
                
                long curTime = System.currentTimeMillis();
                
                HyperPiet.stdoutInfoF.setText("Error: Stack overflow error. In ~" + Long.toString((curTime - startTime) / 1000) + "s. Timestamp: " + new Timestamp(curTime).toString());
                
                break;
            }
            
            // move to next
            
            gx += dx;
            gy += dy;

            second = data.get(gx).get(gy).p;
            
            for (int i=0; i<4; ++i) {
                if (second.equals(Col.BLACK.getProps())) { // running into wall
                    gx -= dx;
                    gy -= dy;
                    
                    rotateDir(true);
                    
                    gx += dx;
                    gy += dy;
                    
                    second = data.get(gx).get(gy).p;
                } else break;
            }
            
            // calculate shifts for command
            
            int hueShift = second.a - first.a;
            int darkShift = second.b - first.b;
                        
            if (hueShift < 0) hueShift += 6;
            if (darkShift < 0) darkShift += 3;
            
            System.out.printf("%d %d\n", gx, gy);
            
            if ((lastDx == -dx || lastDx == -dy) && hueShift == 0 && darkShift == 0 && lastHueShift == 0 && lastDarkShift == 0) { // caught in termination hunk
                long curTime = System.currentTimeMillis();
                
                HyperPiet.stdoutInfoF.setText("Execution successful! In ~" + Long.toString((curTime - startTime) / 1000) + "s. Timestamp: " + new Timestamp(curTime).toString());
                
                break;
            } else {
                lastDx = dx;
                lastDy = dy;
                lastHueShift = hueShift;
                lastDarkShift = darkShift;

                // commands
                
                if ((hueShift > 0 || darkShift > 0) && !(first.equals(Col.WHITE.getProps())) && !(second.equals(Col.WHITE.getProps()))) {
                    System.out.printf("F %d %d S %d %d\n", first.a, first.b, second.a, second.b);
                    System.out.printf("H %d D %d gx %d gy %d\n", hueShift, darkShift, gx, gy);
                    
                    if (darkShift == 0) {
                        if (hueShift == 1) {
                            int v1 = (int)stk.pop(), v2 = (int)stk.pop();

                            stk.push(v1 + v2);
                        } else if (hueShift == 2) {
                            int v1 = (int)stk.pop(), v2 = (int)stk.pop();

                            stk.push(v1 / v2);
                        } else if (hueShift == 3) {
                            int v1 = (int)stk.pop(), v2 = (int)stk.pop();

                            if (v1 > v2) stk.push(1);
                            else stk.push(0);
                        } else if (hueShift == 4) {
                            int v = (int)stk.pop();

                            stk.push(v);
                            stk.push(v);
                        } else {
                            
                            // read character from STDIN
                            
                            String text = HyperPiet.stdinF.getText();
                            
                            if (text.length() == 0) {
                                long curTime = System.currentTimeMillis();
                                
                                HyperPiet.stdoutInfoF.setText("Error: Expected character input from STDIN. In ~"
                                        + Long.toString((curTime - startTime) / 1000) + "s. Timestamp: " + new Timestamp(curTime).toString());
                                
                                break;
                            }

                            stk.push((int)text.charAt(0));
                            HyperPiet.stdinF.setText(text.substring(1));
                        }
                    } else if (darkShift == 1) {
                        if (hueShift == 0) {
                            System.out.println(size);
                            
                            stk.push(size); // push block size
                        } else if (hueShift == 1) {
                            int v1 = (int)stk.pop(), v2 = (int)stk.pop();

                            stk.push(v1 - v2);
                        } else if (hueShift == 2) {
                            int v1 = (int)stk.pop(), v2 = (int)stk.pop();

                            stk.push(v1 % v2);
                        } else if (hueShift == 3) {
                            
                            // change DP
                            
                            int v = (int)stk.pop();

                            if (v < 0) {
                                for (int i=0; i<v; ++i) {
                                    rotateDir(false);
                                }
                            } else if (v > 0) {
                                for (int i=0; i>v; --i) {
                                    rotateDir(false);
                                }
                            }
                        } else if (hueShift == 4) {
                            
                            // stack roll
                            
                            int rolls = (int)stk.pop();
                            int depth = (int)stk.pop();
                            
                            int top;
                            Stack temp;
                            
                            for (int i=0; i<rolls; ++i) {
                                temp = new Stack();
                                top = (int)stk.pop();
                                
                                for (int j=0; j<depth; ++j) temp.push((int)stk.pop());
                                
                                stk.push(top);
                                
                                while (!temp.isEmpty()) stk.push((int)temp.pop());
                            }
                        } else {
                            HyperPiet.stdoutF.setText(HyperPiet.stdoutF.getText() + Integer.toString((int)stk.pop())); // write out integer
                        }
                    } else {
                        if (hueShift == 0) {
                            stk.pop();
                        } else if (hueShift == 1) {
                            int v1 = (int)stk.pop(), v2 = (int)stk.pop();

                            stk.push(v1 * v2);
                        } else if (hueShift == 2) {
                            
                            // logical not
                            
                            int v = (int)stk.pop();

                            if (v == 0) stk.push(1);
                            else stk.push(0);
                        } else if (hueShift == 3) {
                            // switch
                        } else if (hueShift == 4) {
                            
                            // read integer from STDIN
                            
                            String text = HyperPiet.stdinF.getText();
                            
                            if (isNumeric(text)) {
                                stk.push(Integer.parseInt(text));
                                HyperPiet.stdinF.setText("");
                                
                                System.out.println("[" + text + "]");
                            } else {
                                Matcher m = nlp.matcher(text);
                                
                                if (m.find()) {
                                    stk.push(Integer.parseInt(text.substring(0, m.start())));
                                    HyperPiet.stdinF.setText(text.substring(m.end()));
                                } else {
                                    long curTime = System.currentTimeMillis();

                                    HyperPiet.stdoutInfoF.setText("Error: Expected integer input from STDIN. In ~"
                                            + Long.toString((curTime - startTime) / 1000) + "s. Timestamp: " + new Timestamp(curTime).toString());

                                    break;
                                }
                            }
                        } else {
                            HyperPiet.stdoutF.setText(HyperPiet.stdoutF.getText() + Character.toString((char)(int)stk.pop())); // write out character
                        }
                    }
                }

                first = second;
            }
        }
        
        timeoutCheck.cancel();
    }
}