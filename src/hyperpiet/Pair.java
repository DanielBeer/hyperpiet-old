package hyperpiet;

public class Pair {
    public int a, b;
    
    Pair(int a, int b) {
        this.a = a;
        this.b = b;
    }
    
    public Boolean equals(Pair p) {        
        return this.a == p.a && this.b == p.b;
    }
}