package hyperpiet;

public class Col {
    
    // static variables
    public static final Col WHITE = new Col("#FFFFFF", 6, 0);
    public static final Col BLACK = new Col("#000000", 6, 1);
    
    // instance variables
    String col;
    int hue, darkness;
    
    // constructor
    Col(String col, int hue, int darkness) {
        this.col = col;
        this.hue = hue;
        this.darkness = darkness;
    }
    
    
    public String getCol() {
        return this.col;
    }
    
    
    public Pair getProps() {
        return new Pair(this.hue, this.darkness);
    }
}